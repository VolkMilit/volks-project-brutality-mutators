# Volks Project Brutality Mutators

This mutator trying to make process of killing monsters more satisfing.

### mutator-core

- Decrease monsters health to half (Boss monsters, like Cyberdemon, Mastermind was not touched)
- Increase player base health to 150
- Increase melee weapon damage (PB Knife) x2
- Increase saw damage x2
- Lesser monster energy now gives 2 armor and 5 health point
- Big monster energy gives 5 armor and 10 health
- Soulsphere now add 100 health point (250 maximum)
- Megasphere now adds 250 armor and health points

### mutator-noigotsaw (optional)

- Disable stupid IGOTSAW sound (even throught maunts was disabled in settings)

### mutator-weaponry (optional)

- Increase rocket launcher damage from 20 to 40
- Disable useless BDPistol (zombimans will drop clip for rifle instead)
- Disable Carbine, HMG, PlasmaBeam
- Rifle now in weapon slot 2
- CompactSMG now in weapon slot 3
- BFG now first weapon in weapon slot 8
- Rifle and MiniGun now has separate ammo type; previous implementaion making sense for vanila, but not for PB

### mutator-individualcrosshairs

Give all weapons an individum crosshairs from (G)ZDoom.

### Usage

Core module:

`gzdoom -iwad DOOM2.wad -file "Project Brutality 2.03.pk3" -file mutator-core`

If you want to disable saw sound:

`gzdoom -iwad DOOM2.wad -file "Project Brutality 2.03.pk3" -file mutator-core -file mutator-noigotsaw`

If you want to play with different weapon settings:

`gzdoom -iwad DOOM2.wad -file "Project Brutality 2.03.pk3" -file mutator-core -file mutator-noigotsaw -file mutator-weaponry -file mutator-individualcrosshairs +bd_NoHMGWeapon 1 +bd_NoCarbineWeapon 1 +bd_NoBFGBeamWeapon 1 +bd_UpgradeGL 1 +bd_NoLMG 1`

I also recommended to install this mods:

- [Target Spy](https://forum.zdoom.org/viewtopic.php?t=60784) - see monstes health bars
- [Gearbox](https://forum.zdoom.org/viewtopic.php?t=71086) - weapon and inventory wheel